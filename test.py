import unittest
import os
from app import create_app

class DashboardTestCase(unittest.TestCase):
    """This class represents the Dashboard test case"""
    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_name=os.getenv('APP_SETTINGS'))
        self.client = self.app.test_client

    def test_api_can_get_dashboard(self):
        """Test API can get a dashboard (GET request)."""
        res = self.client().get('/api/v1/incident-management')
        self.assertEqual(res.status_code, 200)

# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()