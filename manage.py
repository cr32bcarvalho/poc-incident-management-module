import os
from dotenv import load_dotenv
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app import create_app
from app.incidents.models import db
from app.models.user import db as user_db

load_dotenv(verbose=True)

if os.getenv('APP_SETTINGS') is None:
    from pathlib import Path
    load_dotenv(dotenv_path='/home/ubuntu/incident-management/.prod_env', verbose=True)

app = create_app(config_name=os.getenv('APP_SETTINGS'))
migrate = Migrate(app, db)
migrate = Migrate(app, user_db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()