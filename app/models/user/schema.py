from app.models.user import ma, User

class UserSchema(ma.SQLAlchemySchema):
  class Meta:
        model = User
        include_fk = True  
  uuid = ma.auto_field()
  public_id = ma.auto_field()
  name = ma.auto_field()
  email = ma.auto_field()
  telephone = ma.auto_field()
  department = ma.auto_field()
  date_modified = ma.auto_field()
  date_created = ma.auto_field()


