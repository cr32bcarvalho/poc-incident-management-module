import uuid 
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from flask_marshmallow import Marshmallow

db = SQLAlchemy() 
ma = Marshmallow()

class UserType(enum.Enum):
    contributors = "contributors"
    admin = "admin"
    engineers = "engineers"

class User(db.Model):
    """This class represents the bucketlist table."""
    __tablename__ = 'users'

    uuid = db.Column(UUID(as_uuid=True), default=lambda: uuid.uuid4().hex, unique=True, primary_key=True, nullable=False)
    public_id = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    telephone = db.Column(db.String(14), nullable=True)
    department = db.Column(db.String(50), nullable=False)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp())

    def __init__(self, name):
        self.name = name

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return User.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return "<User: {}>".format(self.name)