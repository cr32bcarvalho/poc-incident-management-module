from flask import Flask
from flask_cors import CORS, cross_origin
from instance.config import app_config
from app.incidents.routes import incidents_bp
from app.models.user import db as users_db  # blueprint db
from app.incidents.models import db as incidents_db  # blueprint db
from app.dashboard.routes import dashboard_bp


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=False)
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.config.from_object(app_config[config_name])
    users_db.init_app(app)  
    incidents_db.init_app(app)
    with app.app_context():
        users_db.create_all()
        incidents_db.create_all()

        #Dashboard
        app.register_blueprint(dashboard_bp)
        # Incidents
        app.register_blueprint(incidents_bp)
        return app
