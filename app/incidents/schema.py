from datetime import date
from app.incidents.models import ma, Incident
from app.models.user.schema import UserSchema
from app.models.user import UserType
from marshmallow_enum import EnumField
from flask_marshmallow.fields import URLFor, Hyperlinks

class IncidentSchema(ma.SQLAlchemySchema):
  class Meta:
        model = Incident
        include_fk = True
  uuid = ma.auto_field()
  name = ma.auto_field()
  description = ma.auto_field()
  user = ma.Nested(UserSchema)
  provider = ma.auto_field()
  association_id = ma.auto_field()
  standard_id = ma.auto_field()
  requirement_id = ma.auto_field()
  status = EnumField(UserType)
  is_public = ma.auto_field()
  deadline = ma.auto_field()
  date_modified = ma.auto_field()
  date_created = ma.auto_field()
  _links = Hyperlinks({
    'self': URLFor('incidents_bp.get', uuid='<uuid>'),
    'collection': URLFor('incidents_bp.get'),
  })


