import uuid 
import json
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey, Enum
from app.models.user import User
from flask_marshmallow import Marshmallow

db = SQLAlchemy() 
ma = Marshmallow()

class Status(enum.Enum):
    analysis = 'analysis'
    approved = 'approved'
    solved = 'solved'

class Incident(db.Model):
    """This class represents the bucketlist table."""
    __tablename__ = 'incidents'

    uuid = db.Column(UUID(as_uuid=True), default=lambda: uuid.uuid4().hex, unique=True, primary_key=True, nullable=False)
    user_uuid = db.Column(UUID(as_uuid=True), ForeignKey(User.uuid))
    user = relationship(User)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    provider = db.Column(db.String(100), nullable=True)
    association_id = db.Column(UUID(as_uuid=True), nullable=True)
    standard_id = db.Column(UUID(as_uuid=True), nullable=True)
    requirement_id = db.Column(UUID(as_uuid=True), nullable=True)
    status = db.Column(Enum(Status), default=Status.analysis)
    is_public = db.Column(db.Boolean, default=False, nullable=True)
    deadline = db.Column(db.DateTime, nullable=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp())

    def __init__(self, name):
        self.name = name

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get(uuid):
        return Incident.query.filter(uuid=uuid).all()

    @staticmethod
    def get_all():
        return Incident.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return "<Incidents: {}>".format(self.name)