from flask import Blueprint
from flask import current_app as app
from flask import abort, jsonify, request, Response
from flask_cors import cross_origin
from .models import db, Incident, Status
from .schema import IncidentSchema
from app.authorizor.token import token_required
from app.models.user import UserType

incidents_bp = Blueprint('incidents_bp', __name__, url_prefix='/api/v1/incident-management')

@incidents_bp.route('/incidents', defaults={'uuid': None}, methods=['GET'])
@incidents_bp.route('/incidents/<uuid>', methods=['GET'])
@cross_origin()
@token_required()
def get(current_user, uuid):
    if uuid is None:
        if current_user.department == UserType.contributors:
            incidents = Incident.query.filter_by(user_uuid=current_user.uuid).all()
        else:
            incidents = Incident.query.all()
        schema = IncidentSchema(many=True)
        result = schema.dump(incidents)
        return {"incidents": result}, 200
    schema = IncidentSchema()
    incident = Incident.query.filter_by(uuid=uuid).first_or_404()
    return schema.dump(incident), 200


@incidents_bp.route('/incidents', methods=['POST'])
@cross_origin()
@token_required()
def post(current_user):
    data = request.get_json()
    if data.get('uuid'):
        abort(404)

    if data.get('name') is None or data.get('description') is None:
        abort(400)
    
    incident = Incident(name = data.get('name'))
    incident.description = data.get('description')
    incident.user_uuid = current_user.uuid
    if current_user.department == UserType.engineers.value:
        if data.get('provider') is not None:
            incident.provider = data.get('provider')
        if data.get('association_id') is not None:
            incident.association_id = data.get('association_id')
        if data.get('standard_id') is not None:
            incident.standard_id = data.get('standard_id')
        if data.get('requirement_id') is not None:
            incident.requirement_id = data.get('requirement_id')
        if data.get('status') is not None:
            incident.status = data.get('status')
        if data.get('is_public') is not None:
            incident.is_public = data.get('is_public')
        if data.get('deadline') is not None:
            incident.deadline = data.get('deadline')
    db.session.add(incident)
    db.session.commit()
    return Response(None, status=201, mimetype='application/json')

@incidents_bp.route('/incidents', methods=['PUT'])
@cross_origin()
@token_required()
def put(current_user):
    data = request.get_json()
    if data.get('uuid') is None:
        abort(405)
    incident = Incident.query.filter_by(uuid=data.get('uuid'), user_uuid=current_user.uuid).first_or_404()
    incident.name = data.get('name')
    incident.description = data.get('description')
    if current_user.department == UserType.engineers.value:
        if data.get('provider') is not None:
            incident.provider = data.get('provider')
        if data.get('association_id') is not None:
            incident.association_id = data.get('association_id')
        if data.get('standard_id') is not None:
            incident.standard_id = data.get('standard_id')
        if data.get('requirement_id') is not None:
            incident.requirement_id = data.get('requirement_id')
        if data.get('status') is not None:
            incident.status = data.get('status')
        if data.get('is_public') is not None:
            incident.is_public = data.get('is_public')
        if data.get('deadline') is not None:
            incident.deadline = data.get('deadline')
    db.session.commit()
    return Response(None, status=204, mimetype='application/json')

@incidents_bp.route('/incidents', methods=['PATCH'])
@cross_origin()
@token_required(role=UserType.engineers.value)
def patch(current_user):
    data = request.get_json()
    if data.get('uuid') is None:
        abort(405)
    incident = Incident.query.filter_by(uuid=data.get('uuid')).first_or_404()
    if data.get('provider') is not None:
        incident.provider = data.get('provider')
    if data.get('association_id') is not None:
        incident.association_id = data.get('association_id')
    if data.get('standard_id') is not None:
        incident.standard_id = data.get('standard_id')
    if data.get('requirement_id') is not None:
        incident.requirement_id = data.get('requirement_id')
    if data.get('status') is not None:
        incident.status = data.get('status')
    if data.get('is_public') is not None:
        incident.is_public = data.get('is_public')
    if data.get('deadline') is not None:
        incident.deadline = data.get('deadline')
    db.session.commit()
    return Response(None, status=204, mimetype='application/json')

@incidents_bp.route('/incidents/<uuid>', methods=['DELETE'])
@cross_origin()
@token_required()
def delete(current_user, uuid):
    if uuid is None:
        abort(405)

    incident = Incident.query.filter_by(uuid=uuid, user_uuid=current_user.uuid).first_or_404()
    if incident.status == None or incident.status == Status.analysis:
        db.session.delete(incident)
        db.session.commit()
        return Response(None, status=204, mimetype='application/json')
    return abort(405)