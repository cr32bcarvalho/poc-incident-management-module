from flask import Blueprint
from flask import current_app as app
from flask import abort, jsonify, request
from flask_cors import cross_origin
from app.incidents.models import Incident, Status
from app.incidents.schema import IncidentSchema
from app.models.user import UserType
from app.authorizor.token import token_required

dashboard_bp = Blueprint('dashboard_bp', __name__, url_prefix='/api/v1')
limit = 5
@dashboard_bp.route('/incident-management', methods=['GET'])
@cross_origin()
@token_required()
def dashboard(current_user):
    user_incidents = Incident.query.filter_by(user_uuid=current_user.uuid).filter(Incident.status != Status.analysis).limit(limit).all()
    analyses_incidents = Incident.query.filter_by(status=Status.analysis).limit(limit).all()

    schema = IncidentSchema(many=True)
    user_incidents_result = schema.dump(user_incidents) if user_incidents is not None else []
    analyses_incidents_result = schema.dump(analyses_incidents) if analyses_incidents is not None else []

    return {"incidents": user_incidents_result, "for_analysis": analyses_incidents_result}, 200
