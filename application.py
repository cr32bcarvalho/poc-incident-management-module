import os

from app import create_app
from dotenv import load_dotenv

load_dotenv(verbose=True)

if os.getenv('APP_SETTINGS') is None:
    from pathlib import Path
    load_dotenv(dotenv_path='/home/ubuntu/incident-management/.prod_env')

config_name = os.getenv('APP_SETTINGS')
application = create_app(config_name)

if __name__ == '__main__':
    application.run()